//
//  ViewController.m
//  Multicast
//
//  Created by cfans on 2/25/17.
//  Copyright © 2017 cfans. All rights reserved.
//

#import "ViewController.h"
#import "GCDAsyncUdpSocket.h"

NSString * MULTI_HOST = @"224.11.12.13";
NSUInteger MULTI_PORT = 8989;


@interface ViewController ()<GCDAsyncUdpSocketDelegate>{
    GCDAsyncUdpSocket * udpSocket;
}

@property (weak, nonatomic) IBOutlet UILabel *labelRev;
@property (weak, nonatomic) IBOutlet UITextView *tvSend;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSocket];
}

- (IBAction)doJoin:(id)sender {

    NSError *error = nil;
    if(![udpSocket joinMulticastGroup:MULTI_HOST error:&error]){
        NSLog(@"Error connecting to multicast group: %@", error);
        return;
    }
    if (![udpSocket beginReceiving:&error])
    {
        NSLog(@"Error receiving: %@", error);
        return;
    }
    NSLog(@"Socket Ready");
}


- (IBAction)doLeave:(id)sender {

    NSError *error = nil;
    if(![udpSocket leaveMulticastGroup:MULTI_HOST error:&error]){
        NSLog(@"Error leaveMulticastGroup: %@", error);
        return ;
    }
}

- (IBAction)doSend:(id)sender {
    NSString * str = _tvSend.text;
    NSData * data = [str dataUsingEncoding:NSUTF8StringEncoding];
    [udpSocket sendData:data toHost:MULTI_HOST port:MULTI_PORT withTimeout:1 tag:0];
}

- (void)setupSocket
{
    udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    NSError *error = nil;
    if (![udpSocket bindToPort:MULTI_PORT error:&error])
    {
        NSLog(@"Error binding to port: %@", error);
        return;
    }

//    if(![udpSocket joinMulticastGroup:MULTI_HOST error:&error]){
//        NSLog(@"Error connecting to multicast group: %@", error);
//        return;
//    }
//    if (![udpSocket beginReceiving:&error])
//    {
//        NSLog(@"Error receiving: %@", error);
//        return;
//    }
//    NSLog(@"Socket Ready");

}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext
{



    NSString * msg = [NSString stringWithFormat:@"Recv length: %ld ,%@",(unsigned long)[data length],[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
    _labelRev.text = msg;

    NSString *host = nil;
    uint16_t port = 0;
    [GCDAsyncUdpSocket getHost:&host port:&port fromAddress:address];
    NSLog(@"message from : %@:%hu", host, port);

}

-(void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag{

}

-(void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error{
    NSLog(@"didNotSendDataWithTag : %@:%ld", error, tag);
}

@end
