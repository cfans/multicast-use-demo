//
//  main.m
//  Multicast
//
//  Created by cfans on 2/25/17.
//  Copyright © 2017 cfans. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
