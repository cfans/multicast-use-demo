//
//  AppDelegate.h
//  Multicast
//
//  Created by cfans on 2/25/17.
//  Copyright © 2017 cfans. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

